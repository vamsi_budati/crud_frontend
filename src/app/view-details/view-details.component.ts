import { Component, OnInit } from '@angular/core';
import { CommonService } from './../common.service';

@Component({
  selector: 'app-view-details',
  templateUrl: './view-details.component.html',
  styleUrls: ['./view-details.component.css']
})
export class ViewDetailsComponent implements OnInit {
  d: any;

  constructor(public service: CommonService) { }
  fetchDetails() {
    console.log('hello');
    const url = 'http://localhost:3000/crud/api/select';

    this.service.getDetails(url, null).subscribe(data => {
      this.d = data.data;
      console.log(this.d);

    });
  }

  ngOnInit() {
    this.fetchDetails();
  }

}
