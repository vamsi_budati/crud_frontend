import { Injectable } from '@angular/core';
import { Http, RequestOptionsArgs, ResponseType, ResponseContentType, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';

import 'rxjs/add/operator/map';

@Injectable()
export class CommonService {

  constructor(private http: Http) {}
  getDetails(url, data): Observable<any> {
    console.log('hello');
    return this.http.post(url, null).map((res: Response) => {
      const a = res.json();
      return a;
    });
  }

  insertDetails(url, data): Observable<any> {
    return this.http.post(url, data).map((res: Response) => {
      const c = res.json();
      return c;
    });
  }
}
