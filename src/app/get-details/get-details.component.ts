import { CommonService } from './../common.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-get-details',
  templateUrl: './get-details.component.html',
  styleUrls: ['./get-details.component.css']
})
export class GetDetailsComponent implements OnInit {
  d = [];
  a = {
    first_name: '',
   last_name: '',
   email_id: '',
   phone_no: '',
   gender: '',
   date_of_birth: ''
  };

 details = {
   first_name: '',
   last_name: '',
   email_id: '',
   phone_no: '',
   gender: '',
   date_of_birth: ''
 };
 deleteid = {
   email_id: ''
 };
  constructor(public service: CommonService) {}
  fetchDetails() {
    console.log('hello');
    const url = 'http://localhost:3000/crud/api/select';

    this.service.getDetails(url, null).subscribe(data => {
      this.d = data.data;
      console.log(this.d);
    });
  }

  viewdetails(info) {
    console.log(info);
    this.details.first_name = info.first_name;
    this.details.last_name = info.last_name;
    this.details.email_id = info.email_id;
    this.details.phone_no = info.phone_no;
    this.details.gender = info.gender;
    this.details.date_of_birth = info.date_of_birth;
  }
  updatedetails() {


    const url = 'http://localhost:3000/crud/api/update';
    this.a.first_name = this.details.first_name;
    this.a.last_name = this.details.last_name;
    this.a.email_id = this.details.email_id;
    this.a.phone_no = this.details.phone_no;
    this.a.gender = this.details.gender;
    this.a.date_of_birth = this.details.date_of_birth;
    this.service.insertDetails(url, this.a).subscribe(data => {
      console.log(data);

      if (data.code == 200) {
          alert('updated successfully');

      } else {
        alert('something went wrong');
      }
    });


  }
  deletedetails(item) {
    const url = 'http://localhost:3000/crud/api/delete';
    this.deleteid.email_id = item.email_id;
    console.log('record deleted that having mail id', this.deleteid);
    this.service.insertDetails(url, this.deleteid).subscribe(data => {
      console.log(data);

    if (data.code == 200) {
      alert('record deleted successfully');
    } else {
      alert('something went wrong');
    }
    });
  }



  ngOnInit() {
    this.fetchDetails();
  }
}
