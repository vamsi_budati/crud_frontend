import { CommonService } from './common.service';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {RouterModule, Router} from '@angular/router';

import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { GetDetailsComponent } from './get-details/get-details.component';
import {HttpModule} from '@angular/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {ToastsManager, ToastModule, ToastOptions} from 'ng2-toastr/ng2-toastr';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { CustomOption } from '../custom-options';
import { ViewDetailsComponent } from './view-details/view-details.component';
@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    GetDetailsComponent,
    ViewDetailsComponent
  ],
  imports: [
    BrowserModule,
    HttpModule,
    FormsModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    ToastModule.forRoot(),
    RouterModule.forRoot([
      {path: '', redirectTo: 'home', pathMatch: 'full'},
      {path: 'home', component: HomeComponent},
      {path: 'getdetails', component: GetDetailsComponent},
      {path: 'viewdetails', component: ViewDetailsComponent}
    ])
  ],
  providers: [{provide: ToastOptions, useClass: CustomOption}, CommonService],
  bootstrap: [AppComponent]
})
export class AppModule { }
