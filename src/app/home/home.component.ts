import { CommonService } from './../common.service';
import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { ToastsManager } from 'ng2-toastr/ng2-toastr';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  // payLoad = {
  //   first_name: '',
  //   last_name: '',
  //   email_id: '',
  //   phone_no: '',
  //   date_of_birth: ''
  // };
  details: any;
  constructor(public service: CommonService, public toastr: ToastsManager, vcr: ViewContainerRef) {
    this.toastr.setRootViewContainerRef(vcr);
    this.details = {

    };
   }
  clickSubmit() {
    const url = 'http://localhost:3000/crud/api/insert';
    this.service.insertDetails(url, this.details).subscribe((data) => {
      console.log(data);

        if (data.code === 200) {
          alert('user inserted successfully');
          console.log(data.data);
        } else {
          alert('something went wrong');
        }
    });
  }

  ngOnInit() {
  }

}
